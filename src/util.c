#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "util.h"
#include "constants.h"

int util_path_next_dir_name(HeapData path, int start, HeapData* name) {
	if (!path.valid) return ERR_INVALID_MEMORY_ACCESS;
	if (start < 0 || start > path.size) return ERR_INVALID_MEMORY_ACCESS;
	int error = 0;
	int current_name_len = 0;

	for (int i = start; i < path.size; i++) {
		uint8_t byte = mem_read(path, i, &error);
		if (error != SUCCESS) return error;
		
		// Check for forward slash
		if (byte == 47) {
			break;
		}

		current_name_len += 1;
		// If code exits loop normally, then it is the end of the string
		// which terminates the current dir name (in addition to a forwards 
		// slash)
	}
	int ret = mem_alloc(name, current_name_len);
	if (ret != SUCCESS) return ret;
	
	memcpy(name->data, &path.data[start], current_name_len);
	name->size = current_name_len;
	return SUCCESS;
}

int util_string_to_heap(char* string, HeapData* heap) {
	if (string == NULL) return ERR_NULL_STRING;

	int len = strlen(string);
	if (!len) return ERR_EMPTY_STRING;

	int ret = mem_alloc(heap, len);
	if (ret != SUCCESS) return ret;

	for (int i = 0; i < len; i++) {
		ret = mem_write(heap, i, (uint8_t)string[i]);
		if (ret != SUCCESS) return ret;
	}

	return SUCCESS;
}

uint32_t round_up_nearest_multiple(uint32_t value, uint32_t multiple)
{
	return (uint32_t) multiple * (ceil((double)value/ (double)multiple));
}

int compare_superblock(Superblock s1, Superblock s2){
	int comparison = 0;		// Increment for each difference

	comparison += (s1.magic_1 == s2.magic_1);
	comparison += (s1.version == s2.version);
	comparison += (s1.block_size == s2.block_size);
	comparison += (s1.num_blocks == s2.num_blocks);
	comparison += (s1.num_used_blocks == s2.num_used_blocks);
	comparison += (s1.num_data_blocks == s2.num_data_blocks);
	comparison += (s1.inode_size == s2.inode_size);
	comparison += (s1.num_inodes == s2.num_inodes);
	comparison += (s1.num_used_inodes == s2.num_used_inodes);
	comparison += (s1.inode_bitmap_size == s2.inode_bitmap_size);
	comparison += (s1.inode_table_size == s2.inode_table_size);
	comparison += (s1.data_block_bitmap_size == s2.data_block_bitmap_size);
	comparison += (s1.inode_table_start_addr == s2.inode_table_start_addr);
	comparison += (s1.data_block_bitmap_addr == s2.data_block_bitmap_addr);
	comparison += (s1.data_blocks_start_addr == s2.data_blocks_start_addr);
	comparison += (s1.data_bitmap_circular_loc == s2.data_bitmap_circular_loc);
	comparison += (s1.flags == s2.flags);
	comparison += (s1.magic_1 == s2.magic_1);

	return abs(comparison - 18);
}

int compare_inode(Inode i1, Inode i2) {
	int comparison = 0;
	/*
	comparison +=  (i1.magic == i2.magic);
	comparison +=  (i1.inode_number == i2.inode_number);
	comparison +=  (i1.uid == i2.uid);
	comparison +=  (i1.gid == i2.gid);
	comparison +=  (i1.flags == i2.flags);
	comparison +=  (i1.time_created == i2.time_created);
	comparison +=  (i1.time_last_modified == i2.time_last_modified);
	comparison +=  (i1.size == i2.size);
	comparison +=  (i1.preallocation_size == i2.preallocation_size);

	comparison +=  (i1.data.indirect == i2.data.indirect);
	comparison +=  (i1.data.double_indirect == i2.data.double_indirect);
	comparison +=  (i1.data.triple_indirect == i2.data.triple_indirect);
	*/
	for (int i = 0; i < DIRECT_BLOCK_NUM; i++){
		comparison += (i1.data.direct[i].start_addr == i2.data.direct[i].start_addr);
		comparison += (i1.data.direct[i].length == i2.data.direct[i].length);
	}
	return abs(comparison - 12 - 2 * DIRECT_BLOCK_NUM);

}

int util_write_uint16(HeapData* data, int location, uint16_t value){
	int ret = mem_write(data, location, value >> 8);
	if (ret != SUCCESS) return ret;

	ret = mem_write(data, location + 1, value & 0xFF);
	if (ret != SUCCESS) return ret;

	return SUCCESS;
}

int util_write_uint32(HeapData* data, int location, uint32_t value){
	int ret = util_write_uint16(data, location, value >> 16);
	if (ret != SUCCESS) return ret;

	ret = util_write_uint16(data, location + 2, value & 0xFFFF);
	if (ret != SUCCESS) return ret;

	return SUCCESS;
}

int util_write_uint64(HeapData* data, int location, uint64_t value){
	int ret = util_write_uint32(data, location, value >> 32);
	if (ret != SUCCESS) return ret;

	ret = util_write_uint32(data, location + 4, value & 0xFFFFFFFF);
	if (ret != SUCCESS) return ret;

	return SUCCESS;
}


uint16_t util_read_uint16(HeapData data, int location, int* function_status){
	int status = 0;

	uint8_t msb = mem_read(data, location, &status);
	if (status != SUCCESS){
		*function_status = status;
		return 0;
	}

	uint8_t lsb = mem_read(data, location + 1, &status);
	if (status != SUCCESS){
		*function_status = status;
		return 0;
	}

	*function_status = SUCCESS;
	return (uint16_t)msb << 8 | lsb;
}

uint32_t util_read_uint32(HeapData data, int location, int* function_status)
{
	int status = 0;

	uint16_t msb = util_read_uint16(data, location, &status);
	if (status != SUCCESS)
	{
		*function_status = status;
		return 0;
	} 

	uint16_t lsb = util_read_uint16(data, location + 2, &status);
	if (status != SUCCESS)
	{
		*function_status = status;
		return 0;
	} 

	return (uint32_t)msb << 16 | lsb;
}

uint64_t util_read_uint64(HeapData data, int location, int* function_status)
{
	int status = 0;

	uint32_t msb = util_read_uint32(data, location, &status);
	if (status != SUCCESS)
	{
		*function_status = status;
		return 0;
	} 

	uint32_t lsb = util_read_uint32(data, location + 4, &status);
	if (status != SUCCESS)
	{
		*function_status = status;
		return 0;
	} 

	return (uint64_t)msb << 32 | lsb;
}


void free_element_standard(void* element) {
	free(element);
}

